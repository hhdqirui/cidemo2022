# Defaults apply to all job definitions
default:
  # Use Docker image "maven" with Maven v3.8.x and JDK 11
  image: maven:3.8-jdk-11
  # Directories to cache and key to identify cache
  cache:
    key: mycachekey
    paths:
      - .m2/repository

# While variables are passed to all jobs definitions, 
# they are not defined within "default" section
variables:
  # MAVEN_OPTS are command line parameters passed via the JVM to Maven
  MAVEN_OPTS: "-Dmaven.repo.local=.m2/repository"  

# Define some compile job to be used as build stage (calls Maven)
mycompile:
  stage: build
  # Execute command "mvn compile -–batch mode"
  script:
    - mvn compile --batch-mode
  # We want to keep the files of the build stage so that it can be re-used by test stage
  artifacts:
    paths:
      - "src"
      - "target"
      - "pom.xml"
    expire_in: 1h

# Define some unit test job to be used as (unit) test stage (calls Maven)
myunittest:
  # In Gitlab, stage test   # runs after stage build
  stage: test
  # Prevent any git clone (we import files as artefacts from previous stage). 
  variables:
    GIT_STRATEGY: none
  # Execute mvn test command
  script:
    - mvn test --batch-mode
    # Print JaCoCo result to console log of runner and keep only the footer line (will be parsed by GitLab)
    - cat target/site/jacoco/index.html | grep -o '<tfoot>.*</tfoot>'
  # Let GitLab parse code coverage from runner console and display it in web interface. 
  # The RegEx is greedy and matches the first percentage (which is instruction coverage, not branch coverage)
  # The parsed line is of the format: "Total	6 of 37	83%	1 of 4	75%"	
  coverage: '/Total.*?([0-9]{1,3})%/'
  # Use artifact files from mycompile build phase as input  
  dependencies:
    - mycompile
  artifacts:
    when: always # We want this artifact also when pipeline (=tests) failed.
    reports: # Special report artifact type
      junit:
        - target/surefire-reports/TEST-*.xml
        - target/failsafe-reports/TEST-*.xml
    # We want to keep the JaCoCo report files so that they can be used by pages job in deploy stage
    paths:
      - "src" # mvn site needs also the source files
      - "target"
      - "pom.xml"
    expire_in: 1 h # Just needs to be available for the pages job that runs next

# Web page generation job to be used as deploy stage
# Important: the name of this job must be "pages", otherwise, GitLab will not publish it via GitLab pages!
pages:
  # Gitlab runs stage deploy after stage test
  stage: deploy
  # Prevent any git clone (we import files as artefacts from previous stage). 
  variables:
    GIT_STRATEGY: none
  # Execute command "mvn site --batch mode"
  script:
    - mvn site --batch-mode
    - mkdir public
    - mv target/site/* public/
  artifacts:
    # The generated web pages in directory public needs be exported as artifact
    paths:
      - public
    expire_in: 1 h
  # Use artifact files from myunittest job as input  
  dependencies:
    - myunittest
  only: # Execute only when to main or master branch has been committed 
    - main
    - master
